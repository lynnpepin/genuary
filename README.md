# Genuary 2024

My goal with 2024 is to get comfy using the generative art tools Rust already has.

- Day 1: Particles.
  - I tried twice with Bevy, but it was too verbose + underdocumented to use.
  - Then I went with Nannou and cranked out something good within an hour. I feel good with this.
  - I also did a lot of math for an idea I had. It might come in handy for later. (Vera Molnar,  Chaotic System, Wobbly Function, etc.)
- Day xx: Template
  - I realized I want to copy a significant chunk of boilerplate / setup. This is perfect for a template.
  - I don't plan to retrofit each day with new features I figure to add to the template. (Except maybe something cool, like web export)
  